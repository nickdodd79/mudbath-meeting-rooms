import React from 'react';
import moment from 'moment';

import BigCalendar from 'react-big-calendar';

import 'react-big-calendar/lib/css/react-big-calendar.css';

BigCalendar.momentLocalizer(moment);

// Private functions
function getTitle(booking) {
  const room = this.props.rooms.find(room => {
    return room.id === booking.roomId;
  });

  return `${booking.name} - ${room.name}`;
}

function onSelectDate(date) {

  // Convert back to a moment object
  // If the date is from a slot, then the date is a start property
  date = moment(date.start || date);

  this.props.onSelectDate(date);
}

class BookingCalendar extends React.Component {
  render() {
    return (
      <div className="booking-calendar">        
        <BigCalendar
          selectable
          date={this.props.date.toDate()}
          events={this.props.bookings}
          views={['month', 'week', 'day']}
          getDrilldownView={(date, view) => view}
          titleAccessor={getTitle.bind(this)}
          startAccessor={booking => booking.start.toDate()}
          endAccessor={booking => booking.end.toDate()}
          onNavigate={onSelectDate.bind(this)}
          onSelectSlot={onSelectDate.bind(this)}
          onSelectEvent={this.props.onSelectBooking}
          onSelecting={() => false}
        />
      </div>
    );
  }
}

export default BookingCalendar;
