import React from 'react';

import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import { cyan500 } from 'material-ui/styles/colors';

class BookingListHeader extends React.Component {
  render() {
    return (
      <div className="booking-list-header">
        <Subheader>
          {this.props.label}
        </Subheader>
        <Divider 
          style={
            {
              backgroundColor: cyan500
            }
          } 
        />
      </div>
    );
  }
}

export default BookingListHeader;

            