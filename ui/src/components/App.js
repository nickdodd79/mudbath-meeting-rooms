import React from 'react';
import axios from 'axios';
import moment from 'moment';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import FlatButton from 'material-ui/FlatButton';
import AddIcon from 'material-ui/svg-icons/content/add';

import BookingListHeader from './BookingListHeader';
import BookingList from './BookingList';
import BookingCalendar from './BookingCalendar';
import BookingForm from './BookingForm';

// Private functions
// const baseUrl = 'http://localhost:3001'; //--> Local
const baseUrl = 'http://192.168.99.100:3001'; //--> Docker

function getRooms() {
  return axios.get(`${baseUrl}/rooms`).then(response => {
    return response.data
  });
}

function getMonthBookings(date) {

  // Wrap in a moment to avoid mutation
  const start = moment(date).startOf('month');
  const end = moment(date).endOf('month');

  return buildBookingsRequest(start, end);
}

function getTodayBookings() {
  const today = moment();
  return buildBookingsRequest(today, today);
}

function getTomorrowBookings() {
  const tomorrow = moment().add(1, 'days');
  return buildBookingsRequest(tomorrow, tomorrow);
}

function buildBookingsRequest(start, end) {
  start = start.startOf('day').format('YYYY-MM-DD HH:mm:ss');
  end = end.endOf('day').format('YYYY-MM-DD HH:mm:ss');

  return axios.get(`${baseUrl}/bookings?start=${start}&end=${end}`).then(response => {
    
    // Map the response data to convert the dates to moments
    return response.data.map(booking => {
      return {
        id: booking.id,
        roomId: booking.roomId,
        name: booking.name,
        start: moment(booking.start),
        end: moment(booking.end)
      };
    });
  });
} 

function setDate(date) {

  // If we are selecting a different month then load the new bookings
  const month = date.month();
  const previous = this.state.date.month();
  
  if (month !== previous) {
    getMonthBookings(date).then(bookings => {
      this.setState({
        date: date,
        bookings: bookings
      });
    });  
  } else {
    this.setState({
      date: date
    });
  }    
}

function setBooking(booking) {
  this.setState({
    booking: booking
  });
}

function onSaveBooking(booking) {   
  
  // Update the bookings list based on the save type
  let bookings = this.state.bookings;  
  
  if (this.state.booking.id) {
    bookings = bookings.map(current => {
      return current.id === booking.id ? booking : current;
    }); 
  } else {
    bookings.push(booking);
  }
  
  // Reload the today and tomorrow bookings
  // This could have been resolved on the client, but complications arise with cross-view loading
  // It is therefore easier to get them again
  const today = getTodayBookings();
  const tomorrow = getTomorrowBookings();

  axios.all([today, tomorrow]).then(responses => {
    this.setState({
      bookings: bookings,
      today: responses[0],
      tomorrow: responses[1]
    });

    setBooking.call(this, undefined);
  });
}

function onRemoveBooking() {

  // Update the main bookings state as well as today and tomorrow state
  const filter = bookings => {
    return bookings.filter(booking => {
      return booking.id !== this.state.booking.id
    });
  };

  this.setState({
    bookings: filter(this.state.bookings),
    today: filter(this.state.today),
    tomorrow: filter(this.state.tomorrow)
  });

  setBooking.call(this, undefined);
}

function renderTodayBookings() {
  return renderBookings.call(this, 'today', this.state.today, true);
}

function renderTomorrowBookings() {
  return renderBookings.call(this, 'tomorrow', this.state.tomorrow, false);
}

function renderDateBookings() {
  const bookings = this.state.bookings.filter(booking => {

    // Wrap in a moment to avoid mutation
    const start = moment(this.state.date).startOf('day');
    const end = moment(this.state.date).endOf('day');

    return booking.start.isBetween(start, end);
  });

  return renderBookings.call(this, 'date', bookings, true);
}

function renderBookings(key, bookings, open) {
  return this.state.rooms.map(room => {
    const id = `${key}.${room.id}`;
    const roomBookings = bookings.filter(booking => {
      return booking.roomId === room.id;
    });

    return (
      <BookingList 
        id={id}
        key={id}
        open={open}
        room={room.name} 
        bookings={roomBookings} 
      />
    );
  });
}

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      date: moment(),
      booking: undefined,
      rooms: [],
      bookings: [],
      today: [],
      tomorrow: []
    }
  }

  componentDidMount() {

    // Bootstrap the rooms and bookings data
    const rooms = getRooms();
    const bookings = getMonthBookings(this.state.date);
    const today = getTodayBookings();
    const tomorrow = getTomorrowBookings();

    axios.all([rooms, bookings, today, tomorrow]).then(responses => {
      this.setState({
        rooms: responses[0],
        bookings: responses[1],
        today: responses[2],
        tomorrow: responses[3]
      });
    });
  } 

  render() {
    return (
      <MuiThemeProvider>
        <div>
          <AppBar 
            title="Mudbath Meeting Rooms"
            showMenuIconButton={false} 
            onRightIconButtonClick={() => setBooking.call(this, {})}
            iconElementRight={
              <FlatButton
                label="Add Meeting"
                labelPosition="before"
                primary={true}
                icon={
                  <AddIcon />
                }
              />
            }
          />
          <Drawer 
            open={true}
            containerClassName="booking-list-panel"            
          >
            <BookingListHeader 
              label="Today" 
            />
            {renderTodayBookings.call(this)}            
            <BookingListHeader 
              label="Tomorrow" 
            />
            {renderTomorrowBookings.call(this)}
          </Drawer>
          <BookingCalendar 
            date={this.state.date} 
            rooms={this.state.rooms} 
            bookings={this.state.bookings} 
            onSelectDate={date => setDate.call(this, date)}
            onSelectBooking={booking => setBooking.call(this, booking)}
          />
          <Drawer 
            open={true}
            openSecondary={true}
            containerClassName="booking-list-panel"
          >
            <BookingListHeader 
              label={this.state.date.format('DD MMMM YYYY')} 
            />
            {renderDateBookings.call(this)}  
          </Drawer>
          <BookingForm 
            rooms={this.state.rooms} 
            booking={this.state.booking} 
            onSaveBooking={booking => onSaveBooking.call(this, booking)}
            onRemoveBooking={() => onRemoveBooking.call(this)}
            onCancel={() => setBooking.call(this, undefined)} 
          />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
