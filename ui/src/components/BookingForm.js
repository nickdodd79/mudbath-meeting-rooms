import React from 'react';
import axios from 'axios';
import moment from 'moment';

import Dialog from 'material-ui/Dialog';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import FlatButton from 'material-ui/FlatButton';

// Private functions
// const baseUrl = 'http://localhost:3001'; //--> Local
const baseUrl = 'http://192.168.99.100:3001'; //--> Docker

function serializeTime(date, time) {

  // Merge the date and time
  const value = moment(date).set({
    hour: moment(time).get('hour'),
    minute: moment(time).get('minute'), 
    second: 0
  });

  return value.format('YYYY-MM-DD HH:mm:ss');
}

function onSaveBooking() {
  
  // Check that all the fields have a value
  let errors = {};
  let save = true;
  
  ['roomId', 'name', 'date', 'start', 'end'].forEach(key => {
    if (!this.state[key]) {
      errors[key] = 'A value is required';
      save = false;
    }
  });

  if (save) {

    // Build the booking from the state
    let booking = {
      id: this.state.id,
      roomId: this.state.roomId,
      name: this.state.name,
      start: serializeTime(this.state.date, this.state.start),
      end: serializeTime(this.state.date, this.state.end)
    };

    // Send the booking data to the API
    const action = booking.id 
      ? axios.put(`${baseUrl}/bookings/${booking.id}`, booking)
      : axios.post(`${baseUrl}/bookings`, booking);

    action
      .then(response => {

        // Convert the bookings dates to moments again
        booking = response.data || booking;
        booking = {
          id: booking.id,
          roomId: booking.roomId,
          name: booking.name,
          start: moment(booking.start),
          end: moment(booking.end)
        };

        this.props.onSaveBooking(booking);  
      })
      .catch(context => {

        // Rely on the server to provide any invalid model values
        // A 400 status means the model is invalid
        if (context.response && context.response.status === 400) {
          errors = {};
          
          // If the save results in an error, extract the keys and take the first message for each
          Object.keys(context.response.data).forEach(key => {
            const messages = context.response.data[key];

            if (messages.length > 0) {
              errors[key] = messages[0];
            }
          });

          this.setState({
            errors: errors
          });
        }
      });
  } else {
    this.setState({
      errors: errors
    });
  }
}

function onRemoveBooking() {
  if (this.state.id) {
    axios.delete(`${baseUrl}/bookings/${this.state.id}`).then(() => {
      this.props.onRemoveBooking();
    });
  }
}

function onChange(name, value) {
  const state = {
    errors: {}
  };

  // Dynamically update the state for the named value
  state[name] = value;

  this.setState(state);
}

function renderRooms() {
  return this.props.rooms.map(room => {
    return (
      <MenuItem 
        key={room.id} 
        value={room.id} 
        primaryText={room.name} 
      />
    );
  });
}

function renderFields() {

  // Values can be string or date object, so wrap in moment for consistency
  let date = this.state.date ? this.state.date.toDate() : undefined;
  let start = this.state.start ? this.state.start.toDate() : undefined;
  let end = this.state.end ? this.state.end.toDate() : undefined;

  return (
    <div>
      <SelectField 
        hintText="Room" 
        value={this.state.roomId} 
        fullWidth={true}
        errorText={this.state.errors.roomId} 
        onChange={(event, index, value) => onChange.call(this, 'roomId', value)} 
      >
        {renderRooms.call(this)}
      </SelectField>
      <TextField 
        hintText="Name" 
        value={this.state.name || ''} 
        fullWidth={true}
        errorText={this.state.errors.name} 
        onChange={(event, value) => onChange.call(this, 'name', value)} 
      />
      <DatePicker 
        hintText="Date" 
        autoOk={true}
        value={date} 
        formatDate={date => moment(date).format('DD MMMM YYYY')} 
        errorText={this.state.errors.date} 
        onChange={(event, value) => onChange.call(this, 'date', moment(value))} 
        textFieldStyle={
          {
            width: '100%'
          }
        }
      />
      <TimePicker 
        hintText="Start Time" 
        autoOk={true}
        value={start} 
        minutesStep={5} 
        errorText={this.state.errors.start} 
        onChange={(event, value) => onChange.call(this, 'start', moment(value))} 
        textFieldStyle={
          {
            width: '100%'
          }
        }
      />
      <TimePicker 
        hintText="End Time" 
        autoOk={true}
        value={end} 
        minutesStep={5} 
        errorText={this.state.errors.end} 
        onChange={(event, value) => onChange.call(this, 'end', moment(value))} 
        textFieldStyle={
          {
            width: '100%'
          }
        }
      />
    </div>
  );
}

function renderActions() {
  let actions = (
    <FlatButton
      label="Save"
      primary={true}
      onClick={() => onSaveBooking.call(this)}
    />
  );

  if (this.state.id) {
    actions = (
      <span>
        <FlatButton
          label="Remove"
          secondary={true}
          onClick={() => onRemoveBooking.call(this)}
        />
        {actions}
      </span>
    );
  }

  return (
    <div>
      <FlatButton
        label="Cancel"
        onClick={this.props.onCancel}
      />
      {actions}
    </div>
  );
}

class BookingForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      open: false,
      id: undefined,
      roomId: undefined,
      name: undefined,
      date: undefined,
      start: undefined,
      end: undefined
    }
  }

  componentWillReceiveProps(props) {

    // Ensure the internal state is updated if the parent updates the props
    this.setState({
      errors: {},
      open: !!props.booking,
      id: props.booking ? props.booking.id : undefined,
      roomId: props.booking ? props.booking.roomId : undefined,
      name: props.booking ? props.booking.name : undefined,
      date: props.booking ? props.booking.start : undefined,
      start: props.booking ? props.booking.start : undefined,
      end: props.booking ? props.booking.end : undefined,
    });
  }

  render() {
    return (      
      <Dialog
        title={this.state.id ? 'Edit Booking' : 'Add Booking'}
        modal={false}
        open={!!this.state.open}
        onRequestClose={this.props.onCancel}
        contentStyle={
          {
            width: 350
          }
        }
        actions={
          renderActions.call(this)
        }
      >
        {renderFields.call(this)}
      </Dialog>
    );
  }
}

export default BookingForm;

            