import React from 'react';

import { List, ListItem } from 'material-ui/List';

// Private functions
function renderBookings() {

  // If there are bookings render them else show an empty message
  if (this.props.bookings && this.props.bookings.length > 0) {
    return this.props.bookings.map(booking => {
      return (
        <ListItem
          key={`${this.props.id}.${booking.id}`}
          className="booking-list-item"
          primaryText={booking.name}
          secondaryText={`${booking.start.format('LT')} - ${booking.end.format('LT')}`}
          disabled={true}
        />
      );
    });
  }

  return ([
    <ListItem 
      key={`${this.props.id}.empty`}
      className="booking-list-empty"
      primaryText="No bookings" 
      disabled={true} 
    />
  ]);
}

class BookingList extends React.Component {
  render() {
    return (
      <List>
        <ListItem
          key={`${this.props.id}`}
          primaryText={this.props.room}
          initiallyOpen={!!this.props.open}
          primaryTogglesNestedList={true}
          nestedItems={
            renderBookings.call(this)
          }
        />
      </List>
    );
  }
}

export default BookingList;
