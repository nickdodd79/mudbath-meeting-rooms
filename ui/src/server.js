const express = require('express');

const app = express();
const static = express.static('./build');

app.use(static);
app.listen(3000, () => console.log('Example app listening on port 3000!'));