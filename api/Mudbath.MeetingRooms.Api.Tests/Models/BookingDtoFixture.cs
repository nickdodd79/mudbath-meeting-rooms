﻿using AutoBogus;
using Bogus;
using FluentAssertions;
using Mudbath.MeetingRooms.Api.Data;
using Mudbath.MeetingRooms.Api.Data.Entities;
using Mudbath.MeetingRooms.Api.Models;
using NSubstitute;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Xunit;

namespace Mudbath.MeetingRooms.Api.Tests.Models
{
  public class BookingDtoFixture
  {
    public class RoomId
      : BookingDtoFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<BookingDto, RequiredAttribute>(booking => booking.RoomId);
      }
    }

    public class Name
      : BookingDtoFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<BookingDto, RequiredAttribute>(booking => booking.Name);
      }

      [Fact]
      public void Should_Be_Decorated_With_MaxLengthAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<BookingDto, MaxLengthAttribute>(booking => booking.Name, attribute => attribute.Length == 100);
      }
    }

    public class Start
      : BookingDtoFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<BookingDto, RequiredAttribute>(booking => booking.Start);
      }
    }

    public class End
      : BookingDtoFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<BookingDto, RequiredAttribute>(booking => booking.End);
      }
    }

    public class Validate
      : BookingDtoFixture
    {
      private int _id;
      private BookingDto _dto;
      private IDataContext _dataContext;
      private ValidationContext _validationContext;
      
      public Validate()
      {
        _id = GenerateId();

        _dto = AutoFaker.Generate<BookingDto>();
        _dto.Id = _id;
        _dto.End = _dto.Start.AddDays(1);

        _dataContext = Substitute.For<IDataContext>();

        _validationContext = new ValidationContext(_dto);
      }

      [Fact]
      public void Should_Return_Error_If_Start_Is_After_End()
      {
        _dto.End = _dto.Start.AddDays(-1);

        var result = new ValidationResult("The booking time is invalid", new[] { "start", "end" });

        _dto.Validate(_dataContext, _validationContext).Should().BeEquivalentTo(new[] { result });
      }

      [Fact]
      public void Should_Return_Error_If_Start_Is_Same_As_End()
      {
        _dto.End = _dto.Start;

        var result = new ValidationResult("The booking time is invalid", new[] { "start", "end" });

        _dto.Validate(_dataContext, _validationContext).Should().BeEquivalentTo(new[] { result });
      }

      [Fact]
      public void Should_Return_Error_If_Booking_Overlaps()
      {
        var id = GenerateId();
        var booking = MapBooking(id);
        
        _dataContext.GetBookingsAsync(_dto.RoomId, _dto.Start, _dto.End).ReturnsAsync(new[] { booking });

        var result = new ValidationResult("The booking time overlaps with another", new[] { "start", "end" });

        _dto.Validate(_dataContext, _validationContext).Should().BeEquivalentTo(new[] { result });
      }

      [Fact]
      public void Should_Return_Success_If_Booking_Overlaps_But_Is_Same_Booking()
      {
        var booking = MapBooking();

        _dataContext.GetBookingsAsync(_dto.RoomId, _dto.Start, _dto.End).ReturnsAsync(new[] { booking });

        _dto.Validate(_dataContext, _validationContext).Should().BeEquivalentTo(new[] { ValidationResult.Success });
      }

      [Fact]
      public void Should_Return_Success_If_Booking_Is_Valid()
      {
        _dto.Validate(_dataContext, _validationContext).Should().BeEquivalentTo(new[] { ValidationResult.Success });
      }

      private int GenerateId()
      {
        var faker = new Faker();
        return faker.Random.Number(10, 20);
      }

      private Booking MapBooking(int? id = null)
      {
        return new Booking
        {
          Id = id ?? _id,
          RoomId = _dto.RoomId,
          Name = _dto.Name,
          Start = _dto.Start,
          End = _dto.End
        };
      }
    }
  }
}
