﻿using AutoBogus;
using Bogus;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Mudbath.MeetingRooms.Api.Data;
using Mudbath.MeetingRooms.Api.Data.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

/*
 * NOTE: Due to mocking complications these are more integration tests involving an in memory database
 */

namespace Mudbath.MeetingRooms.Api.Tests.Data
{
  public class DataContextFixture
  {
    private Faker _faker;
    private Room _room;
    private Booking _booking;
    private DataContext _dataContext;

    public DataContextFixture(string databaseName, ITestOutputHelper output)
    {
      Output = output;

      // A unique database is required for each testing concern, so get them to provide their database name
      // This is because the same database is shared by name across multiple tests
      var builder = new DbContextOptionsBuilder<DataContext>().UseInMemoryDatabase(databaseName);

      _faker = new Faker();

      _room = GenerateRoom();
      _booking = GenerateBooking(_room.Id);
      
      _dataContext = new DataContext(builder.Options);

      // Can't use async/await in constructors
      ClearData().Wait();
      InsertData(_room, _booking).Wait();
    }

    private ITestOutputHelper Output { get; }

    public class GetRooms
      : DataContextFixture
    {
      public GetRooms(ITestOutputHelper output)
        : base("GetRooms", output)
      { }

      [Fact]
      public async Task Should_Return_All_Rooms()
      {
        var rooms = await _dataContext.GetRoomsAsync();

        await AssertRoomsAsync(rooms.ToArray());
      }
    }

    public class GetBookings
      : DataContextFixture
    {
      public GetBookings(ITestOutputHelper output)
        : base("GetBookings", output)
      { }

      [Fact]
      public async Task Should_Return_All_Bookings()
      {
        var bookings = await _dataContext.GetBookingsAsync(null, null, null);

        await AssertBookingsAsync(bookings.ToArray());
      }

      [Fact]
      public async Task Should_Return_All_Room_Bookings()
      {
        // Add another room and booking to ensure filtering occurs
        var room = GenerateRoom();
        var booking = GenerateBooking(room.Id);

        await InsertData(room, booking);

        var bookings = await _dataContext.GetBookingsAsync(room.Id, null, null);
        var roomIds = bookings.Select(current => current.RoomId).ToList();

        roomIds.Should()
          .OnlyHaveUniqueItems().And
          .OnlyContain(roomId => roomId == room.Id);
      }

      [Theory]
      [InlineData(0, 0, false)]
      [InlineData(0, 1, false)]
      [InlineData(0, -1, false)]
      [InlineData(1, 0, false)]
      [InlineData(-1, 0, false)]
      [InlineData(-1, 1, false)]
      [InlineData(1, -1, false)]
      [InlineData(-1, -1, false)]
      [InlineData(1, 1, false)]
      [InlineData(-25, -25, true)]
      [InlineData(25, 25, true)]
      [InlineData(24, 1, true)]
      [InlineData(-1, -24, true)]
      public async Task Should_Return_Bookings_Overlapping_Dates(int startOffset, int endOffset, bool empty)
      {
        var start = _booking.Start.AddHours(startOffset);
        var end = _booking.End.AddHours(endOffset);
        var bookings = await _dataContext.GetBookingsAsync(null, start, end);

        Output.WriteLine($"{startOffset} - {endOffset}");
        Output.WriteLine($"{_booking.Start} - {_booking.End}");
        Output.WriteLine($"{start} - {end}");

        if (empty)
        {
          bookings.Should().BeEmpty();
        }
        else
        {
          await AssertBookingsAsync(bookings.ToArray());
        }
      }
    }

    public class CreateBooking
      : DataContextFixture
    {
      public CreateBooking(ITestOutputHelper output)
        : base("CreateBooking", output)
      { }

      [Fact]
      public async Task Should_Add_Booking()
      {
        var booking = GenerateBooking(_room.Id);

        await _dataContext.CreateBookingAsync(booking);

        await AssertBookingsAsync(_booking, booking);
      }
    }

    public class UpdateBooking
      : DataContextFixture
    {
      public UpdateBooking(ITestOutputHelper output)
        : base("UpdateBooking", output)
      {
        _booking.Name = _faker.Random.Word();
      }

      [Fact]
      public async Task Should_Update_Booking()
      {
        await _dataContext.UpdateBookingAsync(_booking);

        await AssertBookingsAsync(_booking);
      }

      [Fact]
      public async Task Should_Return_True_If_Booking_Updated()
      {
        var result = await _dataContext.UpdateBookingAsync(_booking);

        result.Should().BeTrue();
      }

      [Fact]
      public async Task Should_Return_False_If_Booking_Not_Updated()
      {
        _booking = GenerateBooking(_room.Id);

        var result = await _dataContext.UpdateBookingAsync(_booking);

        result.Should().BeFalse();
      }
    }

    public class RemoveBooking
      : DataContextFixture
    {
      public RemoveBooking(ITestOutputHelper output)
        : base("RemoveBooking", output)
      { }

      [Fact]
      public async Task Should_Update_Booking()
      {
        await _dataContext.RemoveBookingAsync(_booking.Id);

        await AssertBookingsAsync();
      }

      [Fact]
      public async Task Should_Return_True_If_Booking_Updated()
      {
        var result = await _dataContext.RemoveBookingAsync(_booking.Id);

        result.Should().BeTrue();
      }

      [Fact]
      public async Task Should_Return_False_If_Booking_Not_Updated()
      {
        _booking = GenerateBooking(_room.Id);

        var result = await _dataContext.RemoveBookingAsync(_booking.Id);

        result.Should().BeFalse();
      }
    }

    private Room GenerateRoom()
    {
      var room = AutoFaker.Generate<Room>();

      room.Bookings = null;

      return room;
    }

    private Booking GenerateBooking(int roomId)
    {
      var now = DateTime.Now;
      var booking = AutoFaker.Generate<Booking>();

      booking.RoomId = roomId;
      booking.Start = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, 0);
      booking.End = booking.Start.AddDays(1);
      booking.Room = null;

      return booking;
    }

    private async Task AssertRoomsAsync(params Room[] rooms)
    {
      var data = await _dataContext.Rooms.ToListAsync();
      data.Should().BeEquivalentTo(rooms);
    }

    private async Task AssertBookingsAsync(params Booking[] bookings)
    {
      var data = await _dataContext.Bookings.ToListAsync();
      data.Should().BeEquivalentTo(bookings);
    }

    private async Task ClearData()
    {
      // Set up the database with data relevant to all tests
      var rooms = await _dataContext.Rooms.ToListAsync();
      var bookings = await _dataContext.Bookings.ToListAsync();

      // This also involves clearing out any database added during previous test runs
      // This is because the in memory database is persisted across the test runs
      _dataContext.Rooms.RemoveRange(rooms);
      _dataContext.Bookings.RemoveRange(bookings);

      await _dataContext.SaveChangesAsync();
    }

    private async Task InsertData(Room room, Booking booking)
    {
      _dataContext.Rooms.Add(room);
      _dataContext.Bookings.Add(booking);

      await _dataContext.SaveChangesAsync();
    }
  }
}
