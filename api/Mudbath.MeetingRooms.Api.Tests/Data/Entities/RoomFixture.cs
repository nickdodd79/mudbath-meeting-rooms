﻿using Mudbath.MeetingRooms.Api.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xunit;

/*
 * NOTE: These tests verify the schema for EF migrations (annotation attributes)
 */

namespace Mudbath.MeetingRooms.Api.Tests.Data.Entities
{
  public class RoomFixture
  {
    public class Id
      : RoomFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_KeyAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Room, KeyAttribute>(booking => booking.Id);
      }

      [Fact]
      public void Should_Be_Decorated_With_DatabaseGeneratedAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Room, DatabaseGeneratedAttribute>(
          booking => booking.Id, attribute => attribute.DatabaseGeneratedOption == DatabaseGeneratedOption.Identity);
      }
    }

    public class Name
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Room, RequiredAttribute>(booking => booking.Name);
      }

      [Fact]
      public void Should_Be_Decorated_With_MaxLengthAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Room, MaxLengthAttribute>(
          booking => booking.Name, attribute => attribute.Length == 100);
      }
    }

    public class Bookings
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_ForeignKeyAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Room, ForeignKeyAttribute>(
          booking => booking.Bookings, attribute => attribute.Name == "RoomId");
      }
    }
  }
}
