﻿using Mudbath.MeetingRooms.Api.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xunit;

/*
 * NOTE: These tests verify the schema for EF migrations (annotation attributes)
 */

namespace Mudbath.MeetingRooms.Api.Tests.Data.Entities
{
  public class BookingFixture
  {
    public class Id
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_KeyAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, KeyAttribute>(booking => booking.Id);
      }

      [Fact]
      public void Should_Be_Decorated_With_DatabaseGeneratedAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, DatabaseGeneratedAttribute>(booking => booking.Id, attribute => attribute.DatabaseGeneratedOption == DatabaseGeneratedOption.Identity);
      }
    }

    public class RoomId
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, RequiredAttribute>(booking => booking.RoomId);
      }
    }

    public class Name
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, RequiredAttribute>(booking => booking.Name);
      }

      [Fact]
      public void Should_Be_Decorated_With_MaxLengthAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, MaxLengthAttribute>(booking => booking.Name, attribute => attribute.Length == 100);
      }
    }

    public class Start
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, RequiredAttribute>(booking => booking.Start);
      }
    }

    public class End
      : BookingFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RequiredAttribute()
      {
        ReflectionHelper.AssertPropertyDecoration<Booking, RequiredAttribute>(booking => booking.End);
      }
    }
  }
}
