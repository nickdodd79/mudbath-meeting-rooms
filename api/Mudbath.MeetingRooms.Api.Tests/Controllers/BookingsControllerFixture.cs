﻿using AutoBogus;
using Bogus;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Mudbath.MeetingRooms.Api.Controllers;
using Mudbath.MeetingRooms.Api.Data;
using Mudbath.MeetingRooms.Api.Data.Entities;
using Mudbath.MeetingRooms.Api.Models;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

/*
 * NOTE: These tests verify the behaviour and REST contract (verb + url attributes)
 */

namespace Mudbath.MeetingRooms.Api.Tests.Controllers
{
  public class BookingsControllerFixture
  {
    private Faker _faker;
    private List<Booking> _bookings;
    private IDataContext _dataContext;
    private BookingsController _controller;

    public BookingsControllerFixture()
    {
      _faker = new Faker();

      _bookings = AutoFaker.Generate<Booking>(3).ToList();

      _dataContext = Substitute.For<IDataContext>();

      _controller = new BookingsController(_dataContext);
    }

    private int GenerateId()
    {
      return _faker.Random.Number(1, 20);
    }

    public class Class
      : BookingsControllerFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RouteAttribute()
      {
        ReflectionHelper.AssertClassDecoration<BookingsController, RouteAttribute>(attribute => attribute.Template == "bookings");
      }
    }

    public class Constructor
      : BookingsControllerFixture
    {
      [Fact]
      public void Should_Throw_If_DataContext_Is_Null()
      {
        Action action = () => _controller = new BookingsController(null);

        action.Should()
          .Throw<ArgumentNullException>().And
          .ParamName.Should().Be("dataContext");
      }
    }

    public class GetBookings
      : BookingsControllerFixture
    {
      private DateTime _start;
      private DateTime _end;

      public GetBookings()
      {
        var days = _faker.Random.Number(5, 7);
        var timespan = TimeSpan.FromDays(days);

        _start = DateTime.Now;
        _end = _start.Add(timespan);

        _dataContext.GetBookingsAsync(null, _start, _end).ReturnsAsync(_bookings);
      }

      [Fact]
      public void Should_Be_Decorated_With_HttpGetAttribute()
      {
        ReflectionHelper.AssertMethodDecoration<BookingsController, HttpGetAttribute>(
          controller => controller.GetBookings(_start, _end), attribute => attribute.Template == null);
      }

      [Fact]
      public async Task Should_Call_DataContext_GetBookingsAsync()
      {
        await _dataContext.GetBookingsAsync(null, _start, _end);

        await _controller.GetBookings(_start, _end);
      }

      [Fact]
      public async Task Should_Return_Bookings_Json()
      {
        var dtos = _bookings.Select(booking => new BookingDto
        {
          Id = booking.Id,
          RoomId = booking.RoomId,
          Name = booking.Name,
          Start = booking.Start,
          End = booking.End
        });

        var json = new JsonResult(dtos);
        var result = await _controller.GetBookings(_start, _end);
          
        result.Should().BeEquivalentTo(json);
      }
    }

    public class CreateBooking
      : BookingsControllerFixture
    {
      private int _id;
      private BookingDto _dto;
      private string _location;

      public CreateBooking()
      {
        _id = GenerateId();
        _dto = AutoFaker.Generate<BookingDto>();
        _location = _faker.Internet.UrlWithPath();

        ConfigureController();
      }

      [Fact]
      public void Should_Be_Decorated_With_HttpPostAttribute()
      {
        ReflectionHelper.AssertMethodDecoration<BookingsController, HttpPostAttribute>(
          controller => controller.CreateBooking(_dto));
      }

      [Fact]
      public async Task Should_Return_BadRequest_If_Invalid_ModelState()
      {
        _controller.ModelState.AddModelError("Test", "Error");

        var badRequest = new BadRequestObjectResult(_controller.ModelState);
        var result = await _controller.CreateBooking(_dto);
        
        result.Should().BeEquivalentTo(badRequest);
      }

      [Fact]
      public async Task Should_Call_DataContext_CreateBookingAsync_With_Booking()
      {
        Booking booking = null;

        await _dataContext.CreateBookingAsync(Arg.Do<Booking>(arg => booking = arg));

        await _controller.CreateBooking(_dto);

        booking.Should().BeEquivalentTo(new Booking
        {
          RoomId = _dto.RoomId,
          Name = _dto.Name,
          Start = _dto.Start,
          End = _dto.End
        });
      }

      [Fact]
      public async Task Should_Return_Created()
      {
        var dto = new BookingDto
        {
          Id = _id,
          RoomId = _dto.RoomId,
          Name = _dto.Name,
          Start = _dto.Start,
          End = _dto.End
        };

        await _dataContext.CreateBookingAsync(Arg.Do<Booking>(arg => arg.Id = _id));

        var created = new CreatedResult(_location, dto);
        var result = await _controller.CreateBooking(_dto);
        
        result.Should().BeEquivalentTo(created);
      }

      [Fact]
      public async Task Should_Return_Created_With_Port()
      {
        // For local development a port could be involved, but not generally in production
        var port = _faker.Random.Number(1000, 2000);

        ConfigureController(port);

        await _dataContext.CreateBookingAsync(Arg.Do<Booking>(arg => arg.Id = _id));

        var result = await _controller.CreateBooking(_dto) as CreatedResult;
        var location = new Uri(result.Location);

        location.Port.Should().Be(port);
      }

      private void ConfigureController(int? port = null)
      {
        var builder = new UriBuilder(_location);

        if (port.HasValue)
        {
          builder.Port = port.Value;
        }

        // Need to configure the controller context and url helper to build the resource location
        var host = new HostString(builder.Host, builder.Port);
        var httpContext = Substitute.For<HttpContext>();
        var httpRequest = Substitute.For<HttpRequest>();
        var actionContext = new ActionContext
        {
          HttpContext = httpContext,
          RouteData = new RouteData(),
          ActionDescriptor = new ControllerActionDescriptor()
        };

        httpRequest.Scheme.Returns(builder.Scheme);
        httpRequest.Host.Returns(host);

        httpContext.Request.Returns(httpRequest);

        _controller.ControllerContext = new ControllerContext(actionContext);

        _controller.Url = Substitute.For<IUrlHelper>();
        _controller.Url.Content($"~/bookings/{_id}").Returns(builder.Path);
      }
    }

    public class UpdateBooking
      : BookingsControllerFixture
    {
      private int _id;
      private BookingDto _dto;

      public UpdateBooking()
      {
        _id = GenerateId();
        _dto = AutoFaker.Generate<BookingDto>();
      }

      [Fact]
      public void Should_Be_Decorated_With_HttpPutAttribute()
      {
        ReflectionHelper.AssertMethodDecoration<BookingsController, HttpPutAttribute>(
          controller => controller.UpdateBooking(_id, _dto), attribute => attribute.Template == "{id}");
      }

      [Fact]
      public async Task Should_Return_BadRequest_If_Invalid_ModelState()
      {
        _controller.ModelState.AddModelError("Test", "Error");

        var badRequest = new BadRequestObjectResult(_controller.ModelState);
        var result = await _controller.UpdateBooking(_id, _dto);
        
        result.Should().BeEquivalentTo(badRequest);
      }

      [Fact]
      public async Task Should_Call_DataContext_UpdateBookingAsync_With_Booking()
      {
        Booking booking = null;

        await _dataContext.UpdateBookingAsync(Arg.Do<Booking>(arg => booking = arg));

        await _controller.UpdateBooking(_id, _dto);

        booking.Should().BeEquivalentTo(new Booking
        {
          Id = _id,
          RoomId = _dto.RoomId,
          Name = _dto.Name,
          Start = _dto.Start,
          End = _dto.End
        });
      }

      [Fact]
      public async Task Should_Return_Ok_If_Updated()
      {
        _dataContext.UpdateBookingAsync(Arg.Any<Booking>()).ReturnsAsync(true);

        var ok = new OkResult();
        var result = await _controller.UpdateBooking(_id, _dto);
        
        result.Should().BeEquivalentTo(ok);
      }

      [Fact]
      public async Task Should_Return_NotFound_If_Not_Updated()
      {
        _dataContext.UpdateBookingAsync(Arg.Any<Booking>()).ReturnsAsync(false);

        var notFound = new NotFoundResult();
        var result = await _controller.UpdateBooking(_id, _dto);
        
        result.Should().BeEquivalentTo(notFound);
      }
    }

    public class RemoveBooking
      : BookingsControllerFixture
    {
      private int _id;

      public RemoveBooking()
      {
        _id = GenerateId();
      }

      [Fact]
      public void Should_Be_Decorated_With_HttpDeleteAttribute()
      {
        ReflectionHelper.AssertMethodDecoration<BookingsController, HttpDeleteAttribute>(
          controller => controller.RemoveBooking(_id), attribute => attribute.Template == "{id}");
      }

      [Fact]
      public async Task Should_Call_DataContext_RemoveBookingAsync()
      {
        await _controller.RemoveBooking(_id);

        await _dataContext.Received().RemoveBookingAsync(_id);
      }

      [Fact]
      public async Task Should_Return_Ok_If_Removed()
      {
        _dataContext.RemoveBookingAsync(_id).ReturnsAsync(true);

        var ok = new OkResult();
        var result = await _controller.RemoveBooking(_id);
        
        result.Should().BeEquivalentTo(ok);
      }

      [Fact]
      public async Task Should_Return_NotFound_If_Not_Removed()
      {
        _dataContext.RemoveBookingAsync(_id).ReturnsAsync(false);

        var notFound = new NotFoundResult();
        var result = await _controller.RemoveBooking(_id);
        
        result.Should().BeEquivalentTo(notFound);
      }
    }
  }
}
