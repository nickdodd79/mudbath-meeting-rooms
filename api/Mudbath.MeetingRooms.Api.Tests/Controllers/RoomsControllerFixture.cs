﻿using AutoBogus;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Mudbath.MeetingRooms.Api.Controllers;
using Mudbath.MeetingRooms.Api.Data;
using Mudbath.MeetingRooms.Api.Data.Entities;
using Mudbath.MeetingRooms.Api.Models;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

/*
 * NOTE: These tests verify the behaviour and REST contract (verb + url attributes)
 */

namespace Mudbath.MeetingRooms.Api.Tests.Controllers
{
  public class RoomsControllerFixture
  {
    private List<Room> _rooms;
    private IDataContext _dataContext;
    private RoomsController _controller;

    public RoomsControllerFixture()
    {
      _rooms = AutoFaker.Generate<Room>(3).ToList();

      _dataContext = Substitute.For<IDataContext>();
      _dataContext.GetRoomsAsync().Returns(_rooms);

      _controller = new RoomsController(_dataContext);
    }

    public class Class
      : RoomsControllerFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_RouteAttribute()
      {
        ReflectionHelper.AssertClassDecoration<RoomsController, RouteAttribute>(attribute => attribute.Template == "rooms");
      }
    }

    public class Constructor
      : RoomsControllerFixture
    {
      [Fact]
      public void Should_Throw_If_DataContext_Is_Null()
      {
        Action action = () => _controller = new RoomsController(null);

        action.Should()
          .Throw<ArgumentNullException>().And
          .ParamName.Should().Be("dataContext");
      }
    }

    public class GetRooms
      : RoomsControllerFixture
    {
      [Fact]
      public void Should_Be_Decorated_With_HttpGetAttribute()
      {
        ReflectionHelper.AssertMethodDecoration<RoomsController, HttpGetAttribute>(controller => controller.GetRooms(), attribute => attribute.Template == null);
      }

      [Fact]
      public async Task Should_Call_DataContext_GetRoomsAsync()
      {
        await _controller.GetRooms();

        await _dataContext.Received().GetRoomsAsync();
      }

      [Fact]
      public async Task Should_Return_Rooms_Json()
      {
        var dtos = _rooms.Select(room => new RoomDto
        {
          Id = room.Id,
          Name = room.Name
        });
        
        var json = new JsonResult(dtos);
        var result = await _controller.GetRooms();
          
        result.Should().BeEquivalentTo(json);
      }
    }
  }
}
