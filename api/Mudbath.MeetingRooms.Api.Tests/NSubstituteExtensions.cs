﻿using NSubstitute;
using NSubstitute.Core;
using System.Threading.Tasks;

namespace Mudbath.MeetingRooms.Api.Tests
{
  public static class NSubstituteExtensions
  {
    public static ConfiguredCall ReturnsAsync<TReturn>(this Task<TReturn> value, TReturn returnThis)
    {
      var result = Task.FromResult(returnThis);
      return value.Returns(result);
    }
  }
}
