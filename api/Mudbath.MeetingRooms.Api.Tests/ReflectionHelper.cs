﻿using FluentAssertions;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Mudbath.MeetingRooms.Api.Tests
{
  public static class ReflectionHelper
  {
    public static void AssertClassDecoration<TType, TAttribute>(
      Expression<Func<TAttribute, bool>> predicate = null)
      where TAttribute : Attribute
    {
      var type = typeof(TType);

      if (predicate == null)
      {
        type.Should().BeDecoratedWith<TAttribute>();
      }
      else
      {
        type.Should().BeDecoratedWith(predicate);
      }
    }

    public static void AssertMethodDecoration<TType, TAttribute>(
      Expression<Func<TType, object>> method, Expression<Func<TAttribute, bool>> predicate = null)
      where TAttribute : Attribute
    {
      var type = typeof(TType);
      var expression = method.Body as MethodCallExpression;
      var methodInfo = expression?.Method as MethodInfo;

      if (methodInfo == null)
      {
        throw new ArgumentException("Expression is not a method.");
      }
      else if (predicate == null)
      {
        methodInfo.Should().BeDecoratedWith<TAttribute>();
      }
      else
      {
        methodInfo.Should().BeDecoratedWith(predicate);
      }
    }

    public static void AssertPropertyDecoration<TType, TAttribute>(
      Expression<Func<TType, object>> property, Expression<Func<TAttribute, bool>> predicate = null)
      where TAttribute : Attribute
    {
      var type = typeof(TType);
      var expression = (property.Body as MemberExpression) ?? (property.Body as UnaryExpression).Operand as MemberExpression;
      var propertyInfo = expression?.Member as PropertyInfo;

      if (propertyInfo == null)
      {
        throw new ArgumentException("Expression is not a property.");
      }
      else if (predicate == null)
      {
        propertyInfo.Should().BeDecoratedWith<TAttribute>();
      }
      else
      {
        propertyInfo.Should().BeDecoratedWith(predicate);
      }      
    }
  }
}
