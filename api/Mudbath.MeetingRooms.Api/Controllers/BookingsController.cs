﻿using Microsoft.AspNetCore.Mvc;
using Mudbath.MeetingRooms.Api.Data;
using Mudbath.MeetingRooms.Api.Data.Entities;
using Mudbath.MeetingRooms.Api.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

/*
 * NOTE: The internal constructor is used for unit testing - it is internal to prevent the DI from discovering it
 */

namespace Mudbath.MeetingRooms.Api.Controllers
{
  [Route("bookings")]
  public sealed class BookingsController
    : Controller
  {
    public BookingsController(DataContext dataContext)
      : this(dataContext as IDataContext)
    { }

    internal BookingsController(IDataContext dataContext)
    {
      DataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
    }

    private IDataContext DataContext { get; }

    [HttpGet]
    public async Task<JsonResult> GetBookings([FromQuery] DateTime? start, [FromQuery] DateTime? end)
    {
      var bookings = (from booking in await DataContext.GetBookingsAsync(null, start, end)
                      select new BookingDto
                      {
                        Id = booking.Id,
                        RoomId = booking.RoomId,
                        Name = booking.Name,
                        Start = booking.Start,
                        End = booking.End
                      }).ToList();

      return Json(bookings);
    }

    [HttpPost]
    public async Task<IActionResult> CreateBooking([FromBody] BookingDto dto)
    {
      // Check the model is valid based on required values
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      // Map the dto to an entity instance
      // The Id will be resolved as part of CreateBooking()
      var booking = BindBooking(null, dto);

      await DataContext.CreateBookingAsync(booking);
      
      // Map the creation id back on to the dto to be returned
      dto.Id = booking.Id;

      // Also resolve the resource uri to be returned in the response (as an absolute uri)
      // The use of Url.Content() provides the relative url that considers hosting paths
      var builder = new UriBuilder
      {
        Scheme = Request.Scheme,
        Host = Request.Host.Host,
        Path = Url.Content($"~/bookings/{dto.Id}")
      };

      if (Request.Host.Port.HasValue)
      {
        builder.Port = Request.Host.Port.Value;
      }

      return Created(builder.Uri, dto);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateBooking(int id, [FromBody] BookingDto dto)
    {
      // Check the model is valid based on required values
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      // Map the dto to an entity instance and update
      var booking = BindBooking(id, dto);

      if (await DataContext.UpdateBookingAsync(booking))
      {
        return Ok();
      }

      return NotFound();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> RemoveBooking(int id)
    {
      if (await DataContext.RemoveBookingAsync(id))
      {
        return Ok();
      }

      return NotFound();
    }

    private Booking BindBooking(int? id, BookingDto dto)
    {
      var booking = new Booking
      {
        RoomId = dto.RoomId,
        Name = dto.Name,
        Start = dto.Start,
        End = dto.End
      };

      // Only map the id if one is provided
      if (id != null)
      {
        booking.Id = id.Value;
      }

      return booking;
    }
  }
}
