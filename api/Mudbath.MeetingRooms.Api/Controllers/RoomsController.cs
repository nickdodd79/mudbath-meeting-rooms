﻿using Microsoft.AspNetCore.Mvc;
using Mudbath.MeetingRooms.Api.Data;
using Mudbath.MeetingRooms.Api.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

/*
 * NOTE: The internal constructor is used for unit testing - it is internal to prevent the DI from discovering it
 */

namespace Mudbath.MeetingRooms.Api.Controllers
{
  [Route("rooms")]
  public sealed class RoomsController
    : Controller
  {
    public RoomsController(DataContext dataContext)
      : this(dataContext as IDataContext)
    { }

    internal RoomsController(IDataContext dataContext)
    {
      DataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
    }

    private IDataContext DataContext { get; }

    [HttpGet]
    public async Task<JsonResult> GetRooms()
    {
      var rooms = (from room in await DataContext.GetRoomsAsync()
                   select new RoomDto
                   {
                     Id = room.Id,
                     Name = room.Name
                   }).ToList();

      return Json(rooms);
    }
  }  
}
