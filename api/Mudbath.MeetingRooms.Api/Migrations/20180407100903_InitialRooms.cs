﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Mudbath.MeetingRooms.Api.Migrations
{
  public partial class InitialRooms : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      // Disable identities to ensure predictable ids are inserted
      migrationBuilder.Sql("SET IDENTITY_INSERT Rooms ON");

      migrationBuilder.Sql("INSERT INTO [Rooms] ([Id], [Name]) VALUES (1, 'Boardroom')");
      migrationBuilder.Sql("INSERT INTO [Rooms] ([Id], [Name]) VALUES (2, 'Fishbowl')");

      migrationBuilder.Sql("SET IDENTITY_INSERT Rooms OFF");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("DELETE FROM [Rooms]");
    }
  }
}
