﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Mudbath.MeetingRooms.Api.Migrations
{
  public partial class InitialBookings : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      var today = DateTime.Now;
      var tomorrow = today.AddDays(1);
      var nextMonth = today.AddMonths(1);
      var meeting1 = new DateTime(today.Year, today.Month, 2, 9, 0, 0);
      var meeting2 = new DateTime(today.Year, today.Month, 9, 10, 0, 0);
      var meeting3 = new DateTime(today.Year, today.Month, 9, 10, 30, 0);
      var meeting4 = new DateTime(today.Year, today.Month, 12, 9, 0, 0);
      var meeting5 = new DateTime(today.Year, today.Month, 22, 11, 15, 0);
      var meeting6 = new DateTime(today.Year, today.Month, 22, 15, 30, 0);
      var meeting7 = new DateTime(today.Year, today.Month, today.Day, 9, 0, 0);
      var meeting8 = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 12, 45, 0);
      var meeting9 = new DateTime(nextMonth.Year, nextMonth.Month, 4, 10, 0, 0);
      var meeting10 = new DateTime(nextMonth.Year, nextMonth.Month, 22, 13, 15, 0);

      // Disable identities to ensure predictable ids are inserted
      migrationBuilder.Sql("SET IDENTITY_INSERT Bookings ON");

      AddBooking(1, 1, "John Smith", meeting1, 120, migrationBuilder);
      AddBooking(2, 2, "Joe Bloggs", meeting2, 30, migrationBuilder);
      AddBooking(3, 2, "Jane Doe", meeting3, 60, migrationBuilder);
      AddBooking(4, 1, "Kylie Jones", meeting4, 60, migrationBuilder);
      AddBooking(5, 1, "John Smith", meeting5, 15, migrationBuilder);
      AddBooking(6, 2, "Kylie Jones", meeting6, 60, migrationBuilder);
      AddBooking(7, 2, "Kylie Jones", meeting7, 30, migrationBuilder);
      AddBooking(8, 1, "Bill Robinson", meeting8, 30, migrationBuilder);
      AddBooking(9, 1, "John Smith", meeting9, 45, migrationBuilder);
      AddBooking(10, 1, "Jane Doe", meeting10, 90, migrationBuilder);

      migrationBuilder.Sql("SET IDENTITY_INSERT Bookings OFF");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("DELETE FROM [Bookings]");
    }

    private void AddBooking(int id, int roomId, string name, DateTime start, int minutes, MigrationBuilder migrationBuilder)
    {
      var startTime = start.ToString("yyyy-MM-dd HH:mm:ss");
      var endTime = start.AddMinutes(minutes).ToString("yyyy-MM-dd HH:mm:ss");

      migrationBuilder.Sql($"INSERT INTO [Bookings] ([Id], [RoomId], [Name], [Start], [End]) VALUES ({id}, {roomId}, '{name}', '{startTime}', '{endTime}')");
    }
  }
}
