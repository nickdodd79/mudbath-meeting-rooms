﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Mudbath.MeetingRooms.Api.Migrations
{
  public partial class InitialSchema : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "Rooms",
          columns: table => new
          {
            Id = table.Column<int>(nullable: false)
                  .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            Name = table.Column<string>(maxLength: 100, nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Rooms", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Bookings",
          columns: table => new
          {
            Id = table.Column<int>(nullable: false)
                  .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            End = table.Column<DateTime>(nullable: false),
            Name = table.Column<string>(maxLength: 100, nullable: false),
            RoomId = table.Column<int>(nullable: false),
            Start = table.Column<DateTime>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Bookings", x => x.Id);
            table.ForeignKey(
                      name: "FK_Bookings_Rooms_RoomId",
                      column: x => x.RoomId,
                      principalTable: "Rooms",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_Bookings_RoomId",
          table: "Bookings",
          column: "RoomId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "Bookings");

      migrationBuilder.DropTable(
          name: "Rooms");
    }
  }
}
