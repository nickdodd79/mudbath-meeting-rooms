﻿using Mudbath.MeetingRooms.Api.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Mudbath.MeetingRooms.Api.Models
{
  public sealed class BookingDto
    : IValidatableObject
  {
    public int? Id { get; set; }

    [Required]
    public int RoomId { get; set; }

    [Required]
    [MaxLength(100)]
    public string Name { get; set; }

    [Required]
    public DateTime Start { get; set; }

    [Required]
    public DateTime End { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      var dataContext = (IDataContext)validationContext.GetService(typeof(DataContext));
      return Validate(dataContext, validationContext);
    }

    // This method is used for unit testing - so the data context can be mocked
    internal IEnumerable<ValidationResult> Validate(IDataContext dataContext, ValidationContext validationContext)
    {
      var result = ValidationResult.Success;

      // Check the start is before the end
      if (validationContext.ObjectInstance is BookingDto dto)
      {
        if (dto.Start >= dto.End)
        {
          result = new ValidationResult("The booking time is invalid", new[] { "start", "end" });
        }
        else
        {
          var match = dataContext.GetBookingsAsync(dto.RoomId, dto.Start, dto.End);

          // The use of async/await is not supported so the matches need to be extracted from the result
          // However, ignore the overlap if it is the same booking
          if (match.Result.Any(booking => booking.Id != Id))
          {
            result = new ValidationResult("The booking time overlaps with another", new[] { "start", "end" });
          }
        }
      }

      return new[] { result };
    }
  }
}
