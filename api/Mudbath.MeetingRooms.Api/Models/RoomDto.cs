﻿namespace Mudbath.MeetingRooms.Api.Models
{
  public sealed class RoomDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}
