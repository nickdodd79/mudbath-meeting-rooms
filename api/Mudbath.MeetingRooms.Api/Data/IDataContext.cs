﻿using Mudbath.MeetingRooms.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mudbath.MeetingRooms.Api.Data
{
  public interface IDataContext
    : IDisposable
  {
    Task<IEnumerable<Room>> GetRoomsAsync();
    Task<IEnumerable<Booking>> GetBookingsAsync(int? roomId, DateTime? start, DateTime? end);

    Task CreateBookingAsync(Booking booking);
    Task<bool> UpdateBookingAsync(Booking booking);
    Task<bool> RemoveBookingAsync(int id);
  }
}
