﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Mudbath.MeetingRooms.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mudbath.MeetingRooms.Api.Data
{
  public sealed class DataContext
    : DbContext, IDataContext
  {
    public DataContext(DbContextOptions<DataContext> options)
      : base(options)
    { }

    private IConfiguration Configuration { get; }

    public DbSet<Room> Rooms { get; set; }
    public DbSet<Booking> Bookings { get; set; }

    public async Task<IEnumerable<Room>> GetRoomsAsync()
    {
      return await Rooms.ToListAsync();
    }

    public async Task<IEnumerable<Booking>> GetBookingsAsync(int? roomId, DateTime? start, DateTime? end)
    {
      var bookings = Bookings.AsQueryable();

      if (roomId.HasValue)
      {
        bookings = bookings.Where(booking => booking.RoomId == roomId);
      }

      // We can only filter using start and end time if both are provided
      if (start.HasValue && end.HasValue)
      {
        bookings = bookings.Where(booking => booking.Start < end && start < booking.End);
      }

      return await bookings.Include(booking => booking.Room).ToListAsync();
    }

    public async Task CreateBookingAsync(Booking booking)
    {
      Add(booking);
      await SaveChangesAsync();
    }

    public async Task<bool> UpdateBookingAsync(Booking booking)
    {
      var entity = await GetBookingAsync(booking.Id);

      if (entity != null)
      {
        // Map the booking changes to the entity
        // This is done because the entity is already being tracked by EF
        entity.RoomId = booking.RoomId;
        entity.Name = booking.Name;
        entity.Start = booking.Start;
        entity.End = booking.End;

        await SaveChangesAsync();
      }

      return entity != null;
    }

    public async Task<bool> RemoveBookingAsync(int id)
    {
      var entity = await GetBookingAsync(id);

      if (entity != null)
      {
        Remove(entity);
        await SaveChangesAsync();
      }

      return entity != null;
    }

    private async Task<Booking> GetBookingAsync(int id)
    {
      return await Bookings.SingleOrDefaultAsync(booking => booking.Id == id);
    }
  }
}
